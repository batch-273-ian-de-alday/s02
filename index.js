/* 
    Quiz:

    1.  What is the term given to unorganized code that's very hard to work
    with?   
    Answer: Spaghetti Code

    2. How are object literals written in JS?
    Answer: { }

    3. What do you call the concept of organizing information and functionality to belong to an object?
    Answer: Encapsulation

    4. If studentOne has a method named enroll(), how would you invoke it?
    Answer: studentOne.enroll()

    5. True or False: Objects can have objects as properties.
    Answer: True

    6. What is the syntax in creating key-value pairs?
    Answer: key: value

    7. True or False: A method can have no parameters and still work.
    Answer: True

    8. True or False: Arrays can have objects as elements.
    Answer: True

    9. True or False: Arrays are objects.
    Answer: True

    10. True or False: Objects can have arrays as properties.
    Answer: True
*/


// Object Literal Syntax Activity

/* 
1. Translate the other students from our boilerplate code into their own
respective objects. 

2. Define a method for EACH student object that will compute for their
grade average (total of grades divided by 4)

3. Define a method for all student objects named willPass() that returns a Boolean value
indicating if student will pass or fail. For a student to pass, their ave. grade must be greater
than or equal to 85

4. Define a method for all student objects named willPassWithHonors() that returns true
if ave. grade is greater than or equal to 90, false if >= 85 but < 90, and undefined if < 85
(since student will not pass).
*/

let studentOne = {
    name: "John",
    email: "john@mail.com",
    grades: [89, 84, 78, 88],

    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`);
    },
    listGrades() {
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
    },
    getQuarterlyAverage() {
        let sum = 0;
        this.grades.forEach(grade => {
            sum += grade;
        });
        return sum / this.grades.length;
    },
    willPass() {
        return this.getQuarterlyAverage() >= 85;
    },
    willPassWithHonors() {
        if (this.getQuarterlyAverage() >= 90) {
            return true;
        }
        if (this.getQuarterlyAverage() >= 85 && this.getQuarterlyAverage() < 90) {
            return false;
        }
        return undefined
    }
}

console.log("Student One's details: ");
console.log(studentOne.name);
console.log(studentOne.email);
console.log(studentOne.grades);
console.log("Student One's quarterly average: ")
console.log(studentOne.getQuarterlyAverage());
console.log("Will Student One pass? ");
console.log(studentOne.willPass());
console.log("Will Student One pass with honors? ");
console.log(studentOne.willPassWithHonors());


let studentTwo = {
    name: "Joe",
    email: "joe@mail.com",
    grades: [78, 82, 79, 85],

    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`);
    },
    listGrades() {
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
    },
    getQuarterlyAverage() {
        let sum = 0;
        this.grades.forEach(grade => {
            sum += grade;
        });
        return sum / this.grades.length;
    },
    willPass() {
        return this.getQuarterlyAverage() >= 85;
    },
    willPassWithHonors() {
        if (this.getQuarterlyAverage() >= 90) {
            return true;
        }
        if (this.getQuarterlyAverage() >= 85 && this.getQuarterlyAverage() < 90) {
            return false;
        }
        return undefined
    }
}

console.log("Student Two's details: ");
console.log(studentTwo.name);
console.log(studentTwo.email);
console.log(studentTwo.grades);
console.log("Student Two's quarterly average:")
console.log(studentTwo.getQuarterlyAverage());
console.log("Will Student Two pass?");
console.log(studentTwo.willPass());
console.log("Will Student Two pass with honors?");
console.log(studentTwo.willPassWithHonors());


let studentThree = {
    name: "Jane",
    email: "jane@mail.com",
    grades: [87, 89, 91, 93],

    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`);
    },
    listGrades() {
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
    },
    getQuarterlyAverage() {
        let sum = 0;
        this.grades.forEach(grade => {
            sum += grade;
        });
        return sum / this.grades.length;
    },
    willPass() {
        return this.getQuarterlyAverage() >= 85;
    },
    willPassWithHonors() {
        if (this.getQuarterlyAverage() >= 90) {
            return true;
        }
        if (this.getQuarterlyAverage() >= 85 && this.getQuarterlyAverage() < 90) {
            return false;
        }
        return undefined
    }
}

console.log("Student Three's details: ");
console.log(studentThree.name);
console.log(studentThree.email);
console.log(studentThree.grades);
console.log("Student Three's quarterly average:")
console.log(studentThree.getQuarterlyAverage());
console.log("Will Student Three pass?");
console.log(studentThree.willPass());
console.log("Will Student Three pass with honors?");
console.log(studentThree.willPassWithHonors());


let studentFour = {
    name: "Jessie",
    email: "jessie@mail.com",
    grades: [91, 89, 92, 93],

    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`);
    },
    listGrades() {
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
    },
    getQuarterlyAverage() {
        let sum = 0;
        this.grades.forEach(grade => {
            sum += grade;
        });
        return sum / this.grades.length;
    },
    willPass() {
        return this.getQuarterlyAverage() >= 85;
    },
    willPassWithHonors() {
        if (this.getQuarterlyAverage() >= 90) {
            return true;
        }
        if (this.getQuarterlyAverage() >= 85 && this.getQuarterlyAverage() < 90) {
            return false;
        }
        return undefined
    }
}

console.log("Student Four's details: ");
console.log(studentFour.name);
console.log(studentFour.email);
console.log(studentFour.grades);
console.log("Student Four's quarterly average:")
console.log(studentFour.getQuarterlyAverage());
console.log("Will Student Four pass?");
console.log(studentFour.willPass());
console.log("Will Student Four pass with honors?");
console.log(studentFour.willPassWithHonors());


/* 
    5. Create an object named classOf1A with a property named students
    which is an array containing all 4 student objects in it.
    6. Create a method for the object classOf1A named countHonorStudents()
    that will return the number of honor students.

    7. Create a method for the object classOf1A named honorsPercentage()
    that will return the % of honor students from the batch's total number of
    students.

    8. Create a method for the object classOf1A named
    retrieveHonorStudentInfo() that will return all honor students' emails and
    ave. grades as an array of objects.

    9. Create a method for the object classOf1A named
    sortHonorStudentsByGradeDesc() that will return all honor students' emails
    and ave. grades as an array of objects sorted in descending order based on
    their grade averages.
*/

let classOf1A = {
    students: [studentOne, studentTwo, studentThree, studentFour],

    countHonorsStudents() {
        let honorsStudents = 0;
        this.students.forEach(student => {
            if (student.willPassWithHonors() === true) {
                honorsStudents++;
            }
        });
        return honorsStudents;
    },

    honorsPercentage() {
        return this.countHonorsStudents() / this.students.length * 100;
    },
    retrieveHonorStudentInfo() {
        let honorStudents = [];
        this.students.forEach(student => {
            if (student.willPassWithHonors() === true) {
                honorStudents.push({ email: student.email, average: student.getQuarterlyAverage() });
            }
        });
        return honorStudents;
    },

    sortHonorStudentsByGradeDesc() {
        let honorStudents = this.retrieveHonorStudentInfo();
        honorStudents.sort((a, b) => b.average - a.average);
        return honorStudents;
    }
}

console.log("Class of 1A Students: ");
console.log(classOf1A.students);
console.log("Honors Students: ");
console.log(classOf1A.countHonorsStudents());
console.log("Honors Percentage: ");
console.log(classOf1A.honorsPercentage());
console.log("Honor Student Info: ");
console.log(classOf1A.retrieveHonorStudentInfo());
console.log("Honor Students Sorted by Grade Descending: ");
console.log(classOf1A.sortHonorStudentsByGradeDesc());



